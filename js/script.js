$(document).ready(function() {

  $('.footer .callback .code').mask('(000)');
  $('.footer .callback .phone').mask('000-00-00');

    $(function(){
     $('a[href^="#"]').click(function(){
          var target = $(this).attr('href');
          $('html, body').animate({scrollTop: $(target).offset().top-67}, 1000);
          return false; 
     }); 
  });

	$('.partners .tabs_header li a').click(function() {
      $('.partners .tabs_header li').removeClass('active');
      $(this).parent().addClass('active');
      var par_cl = $(this).attr('class');
      $('.partners .tabs_container li').removeClass('active');
      $('.partners .tabs_container li.' + par_cl).addClass('active');
      return false;
   });

	$('.we_offer .tabs_header li a').click(function() {
      $('.we_offer .tabs_header li').removeClass('active');
      $(this).parent().addClass('active');
      var par_cl = $(this).attr('class');
      $('.we_offer .tabs_container li').removeClass('active');
      $('.we_offer .tabs_container li.' + par_cl).addClass('active');
      return false;
   	});



	$('.for_hover').mouseenter(function() {
		var parent_this = $(this).parent();
		$('.for_hover_popup',parent_this).show();
	});

	$('.for_hover').mouseleave(function() {
		var parent_this = $(this).parent();
		$('.for_hover_popup',parent_this).hide();
	});

	$.ajax({
		type: "POST",
		url: 'news_all.html',
		success: function(data) {
	        $('.news .wrapper .all_news').html(data);
			$('.news .flexslider').flexslider({
				animation: "fade",
				animationSpeed: 600,
				directionNav: false,
				slideshow: false
		    });
		    all_news = data;
		}
	});

	$.ajax({
		type: "POST",
		url: 'archiv_news.html',
		success: function(data) {
			$('#modal_arhiv .tabs_container').html(data);
		}
	});

$('.news .title a').click(function() {
        var this_href = $(this).attr('href');
        $('.modal_all_bg').css({visibility:  'visible'});
        $(this_href).css({visibility:  'visible'});
        $('.modal').css({visibility:  'hidden'});
        $('.modal_all_bg').css({visibility:  'hidden'});
        $(this_href).css({visibility:  'visible'});
        $("#modal_arhiv .tabs_container").perfectScrollbar();
        $('.modal_all_bg').css({visibility:  'visible'});
        var Top_modal_window = $(window).height()/2-$(".modal").height()/2;
        var Left_modal_window = $(window).width()/2-$(".modal").width()/2;
        $(".modal").css({"top":Top_modal_window+"px","left":Left_modal_window+"px"});
        return false;
    });

    $(".modal_all_bg, .modal_close").click(function(event) {
        if (event.target.className == 'modal_all_bg' || event.target.className == 'modal_close') {
            $('.modal_all_bg,.modal').css({visibility:  'hidden'});
            // $('.nicescroll-rails').remove();
        }
    });

    $('#modal_arhiv .tabs_header li a').click(function() {
      $('#modal_arhiv .tabs_container').scrollTop(0);
      $("#modal_arhiv .tabs_container").perfectScrollbar();
      $('#modal_arhiv .tabs_header li').removeClass('active');
      $(this).parent().addClass('active');
      var par_cl = $(this).attr('class');
      $('#modal_arhiv .tabs_container li').removeClass('active');
      $('#modal_arhiv .tabs_container li.' + par_cl).addClass('active');
      return false;
   });

$('.news').on('click', 'a.for_click_a', function(){
	var this_href = $(this).attr('href');
	$.ajax({
		type: "POST",
		url: this_href,
		success: function(data) {
			$('#modal_news .show_news').html(data);
			$("#modal_news .text_news").perfectScrollbar();
		}
	});
    $('.modal_all_bg').css({visibility:  'visible'});
    $('#modal_news').css({visibility:  'visible'});
    $('.modal').css({visibility:  'hidden'});
    $('.modal_all_bg').css({visibility:  'hidden'});
    $('#modal_news').css({visibility:  'visible'});
    $('.modal_all_bg').css({visibility:  'visible'});
    var Top_modal_window = $(window).height()/2-$(".modal").height()/2;
    var Left_modal_window = $(window).width()/2-$(".modal").width()/2;
    $(".modal").css({"top":Top_modal_window+"px","left":Left_modal_window+"px"});
    return false;
});

$('#modal_news .go_arh_news a').click(function() {
	$('.modal').css({visibility:  'hidden'});
	$('#modal_arhiv').css({visibility:  'visible'});
	// $('#ascrail2000').remove();
	$("#modal_arhiv .tabs_container").perfectScrollbar();
});

$('#modal_arhiv').on('click', 'a.for_click_a', function(){ 
	var this_href = $(this).attr('href');
	$.ajax({
		type: "POST",
		url: this_href,
		success: function(data) {
			$('#modal_news .show_news').html(data);
			// $('#ascrail2000').remove();
			$("#modal_news .text_news").perfectScrollbar();
		}
	});
    $('.modal_all_bg').css({visibility:  'visible'});
    $('.modal').css({visibility:  'hidden'});
    $('#modal_news').css({visibility:  'visible'});
    
    var Top_modal_window = $(window).height()/2-$(".modal").height()/2;
    var Left_modal_window = $(window).width()/2-$(".modal").width()/2;
    $(".modal").css({"top":Top_modal_window+"px","left":Left_modal_window+"px"});
    return false;
});




});


//  $(window).load(function() {
//     $('.buildings .flexslider').flexslider({
//       animation: "slide",
//       directionNav: false,
//       slideshow: false
//     });
// });